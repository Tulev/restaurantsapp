<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    public $timestamps = false;

    public function restaurants(){
        return $this->belongsToMany(Restaurant::class)
                    ->withPivot('price');
    }

    public function mealType(){
        return $this->belongsTo(MealType::class);
    }
}
