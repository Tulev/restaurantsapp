<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function reviews(){
        return $this->hasMany('App\Review');
    }

    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    public function restaurant(){
        return $this->hasMany(Restaurant::class, 'owner_id');
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
