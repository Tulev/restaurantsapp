<?php


namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    public function index(){
        $users = DB::table('users')->get();

        return view('users.index', compact($users));
    }


}