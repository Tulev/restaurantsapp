<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

    public function restaurant(){
        return $this->hasOne(Restaurant::class);
    }
}
