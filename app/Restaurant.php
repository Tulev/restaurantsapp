<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    public function reviews(){
        return  $this->hasMany(Review::class);
    }

    public function address(){
        return $this->belongsTo(Address::class);
    }

    public function owner(){
        return $this->belongsTo(User::class,'owner_id');
    }

    public function meals(){
        return $this->belongsToMany(Meal::class)
                    ->withPivot('price');
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
