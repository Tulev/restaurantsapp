<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealType extends Model
{
    protected $table = 'meal_type';

    public $timestamps = false;

    public function meals(){
        return $this->hasMany(Meal::class);
    }
}
