<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemOrder extends Model
{
    protected $table = 'item_order';

    public $timestamps = false;

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
