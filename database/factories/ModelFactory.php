<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'phone_number' => $faker->phoneNumber,
    ];
});

$factory->define(App\Review::class, function (Faker\Generator $faker) {
    return [
        'stars'         => $faker->numberBetween(0,9),
        'comment'       => $faker->paragraph(),
        'user_id'       => $faker->numberBetween(1,15),
        'restaurant_id' => $faker->unique($reset = true)->numberBetween(4,9),
    ];
});

$factory->define(App\Restaurant::class, function (Faker\Generator $faker) {
    return [
        'name'     => $faker->company,
        'owner_id'   => '6',
        'address_id'   => $faker->randomDigitNotNull,
        'contact_number' =>$faker->phoneNumber,
    ];
});

$factory->define(App\Address::class, function (Faker\Generator $faker) {
    return [
        'country'   => 'Bulgaria',
        'town'      => 'Sofia',
        'street'    => $faker->streetName,
        'zip'       => $faker->postcode,
      'contact_name'=> $faker->name,
    ];
});

$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        'user_id'            => $faker->numberBetween(1,30),
        'restaurant_id'      => $faker->numberBetween(4,9),
        'order_status_id'    => '1',
    ];
});

$factory->define(App\Meal::class, function (Faker\Generator $faker) {
    return [
        'name'              => $faker->text(20),
        'meal_type_id'      => $faker->numberBetween(1,5),
    ];
});

$factory->define(App\MealType::class, function (Faker\Generator $faker) {
    return [
        //'name' => 'soup',
        //'name' => 'salad',
        //'name' => 'appetizer',
        //'name' => 'main course',
        //'name' => 'dessert',
    ];
});

$factory->define(App\ItemOrder::class, function (Faker\Generator $faker) {
    return [
        'quantity'  => $faker->numberBetween(1,4),
        'price'     => $faker->numberBetween(470,22000),
        'order_id'  => $faker->unique($reset = true)->numberBetween(1,30),
        'meal_id'   => $faker->unique($reset = true)->numberBetween(1,30)
    ];
});

$factory->define(App\OrderStatus::class, function (Faker\Generator $faker) {
    return [
        //'status' => 'processing',
        //'status' => 'completed',
    ];
});

$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        //'name' => 'user'
        //'name' => 'owner'
        //'name' => 'admin'
    ];
});