<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_restaurant', function (Blueprint $table) {
            $table->integer('restaurant_id')->unsigned();
            $table->integer('meal_id')->unsigned();
            $table->integer('price')->unsigned();

            $table->primary(['restaurant_id', 'meal_id']);
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('meal_id')->references('id')->on('meals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meal_restaurant');
    }
}
