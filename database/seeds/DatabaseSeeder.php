<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\User;
use App\Review;
use App\Restaurant;
use App\Address;
use App\Order;
use App\OrderStatus;
use App\MealType;
use App\Role;
use App\Meal;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();



        //factory(User::class, 30)->create();
        //factory(Address::class, 5)->create();
        //factory(Restaurant::class, 3)->create();
        //factory(Review::class, 50)->create();
        //factory(Order::class, 50)->create();
        //factory(OrderStatus::class, 1)->create();

        //factory(MealType::class, 1)->create();
        //factory(Role::class, 1)->create();
        //factory(Meal::class, 50)->create();

        Model::reguard();
    }
}
